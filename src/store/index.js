import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import films from "./modules/films";

export default new Vuex.Store({
  modules: {
    films,
  }
})
