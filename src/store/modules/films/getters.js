export default {
  findFilmById(state) {
    return (id) => {
      return state.source.find(x => x.id === id)
    }
  }
}
