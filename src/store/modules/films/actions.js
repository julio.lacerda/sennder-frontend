import { SET_SOURCE } from './mutation-types'

export default {
  [SET_SOURCE]: ({commit}, payload) => {
    commit(SET_SOURCE, payload)
  }
}
