import { SET_SOURCE } from './mutation-types';

export default {
  [SET_SOURCE] (state, payload) {
    state.source = payload
  },
}
