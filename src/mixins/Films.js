import { SET_SOURCE } from "@/store/modules/films/mutation-types";

export default {
  data() {
    const API_FILMS_BASE_URL = "http://0.0.0.0:8000";
    return {
      polling: null,
      fetchError: null,
      api: {
        films: {
          url: `${API_FILMS_BASE_URL}/films/`
        },
      },
    }
  },
  methods: {
    async fetchFilms() {
      try {
        let response = await this.$axios.get(this.api.films.url)
        if (response.status == 200) {
          this.$store.dispatch(SET_SOURCE, response.data)
        }
        this.fetchError = null;
      } catch(err) {
        this.fetchError = err;
        console.log("Something went wrong when fetching films.", err)
      }
    },
  },
  beforeDestroy () {
    clearInterval(this.polling)
  },
  created() {
    this.fetchFilms()
    this.polling = setInterval(this.fetchFilms, this.$store.state.films.pollingInterval)
  },
}
