# sennder-frontend

## via docker

### build

```
docker build -t sennder-frontend .
```

### run

```
docker run -it -v ${PWD}:/app -v /app/node_modules -p 8080:8080 sennder-frontend
```

## via localhost

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Lints and fixes files
```
npm run lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
